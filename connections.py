import psycopg2
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

def connect_to_db():
    try:
        connection = psycopg2.connect(  user = config['PostgreSQL']['user'], 
                                        password = config['PostgreSQL']['pass'], 
                                        host = config['PostgreSQL']['host'], 
                                        database = config['PostgreSQL']['db'])

    except (Exception, psycopg2.DatabaseError) as error:
        print("ERROR DB: ", error)

    finally:
       return connection



def fetch_data(connection, query):

    try:
        cursor = connection.cursor()
        cursor.execute(query)

        result = cursor.fetchall()

        if result:
            return result
        else:
            return 0

    except (Exception, psycopg2.DatabaseError) as error:
        print("ERROR DB: ", error)

    finally:
        connection.commit()
        
        cursor.close()
        connection.close()




def insert_or_delete(connection, query):
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        
    except (Exception, psycopg2.DatabaseError) as error:
        return 0
        print("ERROR DB: ", error)

    finally:
        connection.commit()
        
        cursor.close()
        connection.close()

    return 1

    connection = connect_to_db()